package com.example.zbl.mytest;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.zbl.mytest.view.StripeAnimTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zbl on 2017/2/27.
 */

public class AnimTestActivity extends AppCompatActivity {

    private Context context;
    private StripeAnimTextView tv_content;
    private Button btn_action;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_animviewtest);
        tv_content = findViewById(R.id.tv_content);
        btn_action = findViewById(R.id.btn_action);
        btn_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }
}
