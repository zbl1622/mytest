package com.example.zbl.mytest.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.Xfermode;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Choreographer;

import com.example.zbl.mytest.utils.DisplayUtil;

/**
 * Created by zbl on 2017/9/25.
 */

public class StripeAnimTextView extends android.support.v7.widget.AppCompatTextView implements Choreographer.FrameCallback {

    private Context context;
    private Choreographer choreographer;
    private Paint paint;
    private Path path, path2;

    private int viewHeight, viewWidth;

    private int stripeWidth = 30;
    private int animateDuration = 500;

    private boolean isRunning = true;
    private long savedStartTime;
    private float process;

    private Xfermode mXfermode;
    private PorterDuff.Mode mPorterDuffMode = PorterDuff.Mode.DST_OUT;

    public StripeAnimTextView(Context context) {
        super(context);
        init(context);
    }

    public StripeAnimTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public StripeAnimTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        choreographer = Choreographer.getInstance();
        stripeWidth = DisplayUtil.dip2Pix(context, 10);

        mXfermode = new PorterDuffXfermode(mPorterDuffMode);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        path = new Path();
        path2 = new Path();

        savedStartTime = System.currentTimeMillis();
        choreographer.postFrameCallback(this);
        isRunning = true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        viewHeight = getHeight();
        viewWidth = getWidth();
        @SuppressLint("DrawAllocation") LinearGradient backGradient = new LinearGradient(0, 0, viewWidth, 0, new int[]{0xFF404EE3, 0xFF598EDE}, null, Shader.TileMode.CLAMP);
        paint.setShader(backGradient);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(0);
        canvas.drawRoundRect(0, 0, viewWidth, viewHeight, viewHeight / 2f, viewHeight / 2f, paint);
        int saveCount = canvas.saveLayer(0, 0, viewWidth, viewHeight, paint);
        //绘制目标图
        paint.setShader(null);
        paint.setStrokeWidth(stripeWidth);
        paint.setColor(0x40000000);
        int count = viewWidth / stripeWidth + 4;
        float offset = process * stripeWidth * 2;
        float offsetX;
        for (int i = 0; i < count; i += 2) {
            offsetX = (i - 1) * stripeWidth + offset;
            canvas.drawLine(offsetX, -stripeWidth, offsetX - stripeWidth * 4, viewHeight + stripeWidth, paint);
        }
        //设置混合模式
        paint.setXfermode(mXfermode);
        //绘制源图
        paint.setShader(null);
        paint.setColor(0xFFFFFFFF);
//        canvas.drawRoundRect(0, 0, viewWidth, viewHeight, viewHeight / 2f, viewHeight / 2f, paint);
        path.reset();
        path2.reset();
        path.addRect(0, 0, viewWidth, viewHeight, Path.Direction.CCW);
        path2.addRoundRect(0, 0, viewWidth, viewHeight, viewHeight / 2f, viewHeight / 2f, Path.Direction.CCW);
        path.op(path2, Path.Op.DIFFERENCE);
        canvas.drawPath(path, paint);
        //清除混合模式
        paint.setXfermode(null);
        //还原画布
        canvas.restoreToCount(saveCount);
        super.onDraw(canvas);
    }

    @Override
    public void doFrame(long frameTimeNanos) {
        if (isRunning) {
            long offsetTime = System.currentTimeMillis() - savedStartTime;
            process = (float) offsetTime / (float) animateDuration;
            process = process - (int) process;
            invalidate();
            choreographer.postFrameCallback(this);

        }
    }
}
